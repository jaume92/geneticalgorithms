//
//  ReadFile.h
//  Salesman
//
//  Jaume Muntsant (1271258)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define N_CITIES 37
#define BARCELONA 3

extern int mat[N_CITIES][N_CITIES];

int readCities(int mat[N_CITIES][N_CITIES]);