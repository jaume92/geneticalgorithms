# Travelling Salesman #

Aquest programa presenta una solució al problema del *travelling salesman* utilitzant algorismes genètics. Ho vaig fer per l'assignatura de Racerca i Innovació del màster de modelització (2016). [Wikipedia](https://en.wikipedia.org/wiki/Travelling_salesman_problem).

**Problema:** Trobar la ruta més curta per les següents ciutats (començant i acabant sempre a Barcelona):
![Captura de pantalla 2016-11-12 a les 17.48.09.png](https://bitbucket.org/repo/ad5p4a/images/3280143668-Captura%20de%20pantalla%202016-11-12%20a%20les%2017.48.09.png)
**Solució:**
![Captura de pantalla 2016-11-12 a les 17.49.42.png](https://bitbucket.org/repo/ad5p4a/images/889262644-Captura%20de%20pantalla%202016-11-12%20a%20les%2017.49.42.png)

### Per executar ###

**1.** Escrivim ``$ make``al terminal per compilar.

**2.** Per executar ``$ ./Salesman: size iterations method``

* *size*: Nombre de individus a cada generació.

* *iterations*: Nombre màxim d'iteracions si no ha convergit el model.

* *method*: Mètode per seleccionar els aparellaments entre individus. El mètode *1* aparella el millor amb el segon millor, després el tercer amb el quart,...
Pel mètode *2* agafem aleatòriament els primers millors individus i després apliquem el mètode *1*. Pel mètode *3* s'agafen les parelles fent servir una funció exponencial de probabilitat, de manera que els millors individus tenen més probabilitat d'aparellar-se.

### Exemple ###

``$ ./Salesman 100 10000 3``

### Resultats ###

L'algorisme s'atura quan troba la distància mínima **16514 km**.

### Llista de ciutats ###

Here is the list of cities:
0. Amsterdam
1. Antwerp
2. Athens
3. Barcelona
4. Berlin
5. Bern
6. Brussels
7. Calais
8. Cologne
9. Copenhagen
10. Edinburgh 
11. Frankfurt 
12. Geneva 
13. Genoa
14. Hamburg 
15. Le Havre 
16. Lisbon
17. London
18. Luxembourg 
19. Lyon
20. Madrid 
21. Marseille 
22. Milan 
23. Munich 
24. Naples 
25. Nice
26. Paris
27. Prague
28. Rome
29. Rotterdam
30. Strasbourg 
31. Stuttgart 
32. The Hague 
33. Turin
34. Venice 
35. Vienna 
36. Zurich