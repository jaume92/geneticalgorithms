//
//  Population.c
//  Salesman
//
//  Jaume Muntsant (1271258)
//
//  Population related functions

#include "ReadFile.h"
#include "Population.h"
#include "Genetic.h"

void initialPop(int **population, int size){
    int i,j,k,rannum, searching;
    int route[N_CITIES+1];
    
    for (i=0; i<size; i++) {
        for (j=0; j<N_CITIES+2; j++) {
            population[i][j]=-1;
        }
    }
    
    
    for (i=0; i<size; i++) {
        population[i][0]=BARCELONA;
        population[i][N_CITIES]=BARCELONA;
    }
    
    for (k=0; k<size; k++) {
        for (i=1; i<N_CITIES; i++) {
            searching=0;
            
            while (searching==0) {
                rannum=rand() % N_CITIES;
                
                for (j=0; j<N_CITIES+1; j++) {
                    if (rannum==population[k][j])
                        break;
                    
                    if (j==N_CITIES) {
                        population[k][i]=rannum;
                        searching=1;
                    }
                }
            }
        }
    }
    
    
    for (j=0; j<size; j++) {
        for (i=0; i<N_CITIES+1; i++) {
            route[i]=population[j][i];
        }
        population[j][N_CITIES+1]=fitness(route);
    }
}


void printPop(int **population, int size){
    int i,j;
    
    for (i=0; i<size; i++) {
        for (j=0; j<N_CITIES+2; j++) {
            printf("%i ",population[i][j]);
        }
        printf("\n");
    }
}

void printVec(int vector[N_CITIES+2]){
    int i;
    
    for(i=0; i<N_CITIES+2; i++){
        printf("%i ",vector[i]);
    }
}


void printMat(){
    int i,j;
    
    for (i=0; i<N_CITIES+1; i++) {
        for (j=0; j<N_CITIES; j++) {
            printf("%i ",mat[i][j]);
        }
        printf("\n");
    }
}


int cmpfunc (const void * a, const void * b){
    return ( *(int*)a - *(int*)b );
}

void sortPop(int **population, int size){
    
    int i,j;
    int fitness[size];
    int fitness_ind[size];
    int provisional[size][N_CITIES+2];
    
    for (i=0; i<size; i++) {
        fitness[i]=population[i][N_CITIES+1];
    }
    
    qsort(fitness, size, sizeof(int), cmpfunc);
    
    for (i=0; i<size; i++) {
        for (j=0; j<size; j++) {
            if (fitness[i]==population[j][N_CITIES+1]) {
                fitness_ind[i]=j;
            }
        }
    }
    
    for (i=0; i<size; i++) {
        for (j=0; j<N_CITIES+2; j++) {
            provisional[i][j]=population[fitness_ind[i]][j];
        }
    }
    
    for (i=0; i<size; i++) {
        for (j=0; j<N_CITIES+2; j++) {
            population[i][j]=provisional[i][j];
        }
    }
    
}