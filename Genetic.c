//
//  Genetic.c
//  Salesman
//
//  Jaume Muntsant (1271258)
//
// Genetic algorithms functions

#include "ReadFile.h"
#include "Genetic.h"
#include "Population.h"

int fitness(int route[N_CITIES+1]){
    int i, fit=0;
    
    for (i=1; i<N_CITIES+1; i++) {
        fit+=mat[route[i-1]+1][route[i]];
    }
    
    return fit;
}


void OX(int **population, int size, int method){
    int i, j, k, l, n=0, rannum1, rannum2, rannumpro, flag=0;
    int pare1[N_CITIES-1], pare2[N_CITIES-1],fill1[N_CITIES-1], fill2[N_CITIES-1],res[N_CITIES-1];
    int route[N_CITIES+1];
    int provisional[size][N_CITIES+2];
    
    if (method==2) {
        tournament(population, size);
    }
    if (method==3) {
        tournament2(population, size);
    }
    
    for (i=0; i<size; i++) {
        for (j=0; j<N_CITIES+2; j++) {
            provisional[i][j]=population[i][j];
        }
    }
    
    for (i=0; i<size-1; i=i+2) {
        rannum1=rand() % (N_CITIES-1);
        rannum2=rand() % (N_CITIES-1);
        
        if (rannum1>rannum2) {
            rannumpro=rannum2;
            rannum2=rannum1;
            rannum1=rannumpro;
        }
        
        for (j=0; j<N_CITIES-1; j++) {
            fill1[j]=-1;
            fill2[j]=-1;
            res[j]=-1;
        }
        
        for (j=0; j<N_CITIES-1; j++) {
            pare1[j]=provisional[i][j+1];
            pare2[j]=provisional[i+1][j+1];
        }
        
        for (j=rannum1; j<=rannum2; j++) {
            fill1[j]=pare1[j];
            fill2[j]=pare1[j];
        }
        
        l=0;
        for (j=0; j<N_CITIES-1; j++) {
            for (k=0; k<N_CITIES-1; k++) {
                if (fill1[k]==pare2[j]) {
                    break;
                }
                
                if (k==N_CITIES-2) {
                    res[l]=pare2[j];
                    l++;
                }
            }
        }
        
        
        for (j=0; j<N_CITIES-1; j++) {
            if (j<rannum1) {
                fill1[j]=res[j];
            }
            
            if (j>=rannum1 && j<=rannum2) {
                continue;
            }
            
            if (j>rannum2) {
                fill1[j]=res[j-(rannum2-rannum1+1)];
            }
        }
        
        for (j=N_CITIES-2; j>=0; j--) {
            if (j>rannum2) {
                fill2[j]=res[N_CITIES-2-j];
            }
            if (j<=rannum2 && j>=rannum1) {
                continue;
            }
            if (j<rannum1) {
                fill2[j]=res[N_CITIES-2-j-(rannum2-rannum1+1)];
            }
        }
        
        population[i][0]=BARCELONA;
        population[i][N_CITIES]=BARCELONA;
        
        for (j=0; j<4; j++) {
            population[n][0]=BARCELONA;
            population[n][N_CITIES]=BARCELONA;
            
            if (j==0) {
                for (k=1; k<N_CITIES; k++) {
                    population[n][k]=pare1[k-1];
                    population[n+1][k]=pare2[k-1];
                    population[n+2][k]=fill1[k-1];
                    population[n+3][k]=fill2[k-1];
                }
            }
            
            n++;
            
            if (n>=size-1) {
                flag=1;
                break;
            }
        }
        
        if(flag==1)
            break;
    }
    
    for (j=0; j<size; j++) {
        for (i=0; i<N_CITIES+1; i++) { //COMPUTE THE FITNESS
            route[i]=population[j][i];
        }
        population[j][N_CITIES+1]=fitness(route);
    }
}


void PIM(int **population, int size){
    int i, j, rannum1, rannum2,rannum3, prov;
    int route[N_CITIES-1];
    
    for (i=size-1; i>=1; i--) {
        rannum1=1+(rand() % (N_CITIES-1));
        rannum2=1+(rand() % (N_CITIES-1));
        
        while (rannum1==rannum2) {
            rannum2=1+(rand() % (N_CITIES-1));
        }
        
        if (population[i][N_CITIES+1]==population[i-1][N_CITIES+1]) {
            prov=population[i][rannum1];
            population[i][rannum1]=population[i][rannum2];
            population[i][rannum2]=prov;
        }else{
            rannum3=rand() % 2;
            
            if (rannum3==0) {
                prov=population[i][rannum1];
                population[i][rannum1]=population[i][rannum2];
                population[i][rannum2]=prov;
            }
        }
    }
    
    for (j=0; j<size; j++) {
        
        for (i=0; i<N_CITIES+1; i++) {
            route[i]=population[j][i];
        }
        population[j][N_CITIES+1]=fitness(route);
    }
    
}


void tournament(int **population, int size){
    int i,j, ran;
    int pool[size/2][N_CITIES+2];
    
    for (i=0; i<size/2; i++) {
        ran=rand() % size;
        
        for (j=0; j<N_CITIES+2; j++) {
            pool[i][j]=population[ran][j];
        }
    }
    
    for (i=size/25; i<size/2; i++) {
        
        for (j=0; j<N_CITIES+2; j++) {
            population[i][j]=pool[i][j];
        }
    }
}

void tournament2(int **population, int size){
    int i,j,k;
    double v[1000], w[1000];
    double interval=0.25;
    int pool[size][N_CITIES+2];
    
    for (i=0; i<size; i++) {
        
        do {
            v[i]= (double) rand()/(RAND_MAX);
            w[i]=-1/0.25*log(v[i]);
        } while (w[i]>=(size/4));
    }
    
    for (i=1; i<=size; i++) {
        
        for (j=0; j<size; j++) {
            
            if (w[j]<(double)i/4 && w[j]>((double)i/4-interval)) {
                
                for (k=0; k<N_CITIES+2; k++) {
                    pool[j][k]=population[i-1][k];
                }
            }
        }
    }
    
    for (i=0; i<size; i++) {
        
        for (j=0; j<N_CITIES+2; j++) {
            population[i][j]=pool[i][j];
        }
    }
}
