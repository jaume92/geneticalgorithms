//
//  ReadFile.c
//  Salesman
//
//  Jaume Muntsant (1271258)
//
//  Read the file

#include "ReadFile.h"

int readCities(int mat[N_CITIES][N_CITIES])
{
    char buffer[305];
    char *record;
    int i=0,j=0;
    FILE *fstream = fopen("taulaciutats.csv","r");
    if(fstream == NULL) {
        fprintf(stderr,"File opening failed\n");
        exit(1) ;
    }
    
    while(fgets(buffer,305,fstream)!=NULL) {
        record = strtok(buffer,";");
        j=0;
        while(record != NULL) {
            if(j == 0){
            }
            else if(i>0){
                if(!strcmp(record,"-")) *record='0';
                mat[i-1][j-1] = atoi(record) ;
            }
            record = strtok(NULL,";");
            j++;
        }
        i++ ;
    }
    fclose(fstream);
    return 0;
}
