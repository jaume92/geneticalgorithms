//
//  Population.h
//  Salesman
//
//  Jaume Muntsant (1271258)

void initialPop(int **population, int size);
void printPop(int **population, int size);
void printMat();
int cmpfunc (const void *a, const void *b);
void sortPop(int **population, int size);
void printVec(int vector[N_CITIES+2]);