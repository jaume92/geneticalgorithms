//
//  Genetic.h
//  Salesman
//
//  Jaume Muntsant (1271258)

int fitness(int route[N_CITIES+1]);
void OX(int **population, int size, int method);
void PIM(int **population, int size);
void tournament(int **population, int size);
void tournament2(int **population, int size);