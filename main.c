//
//  main.c
//  Salesman
//
//  Jaume Muntsant (1271258)
//
//  Main function

#include "ReadFile.h"
#include "Population.h"
#include "Genetic.h"

int mat[N_CITIES][N_CITIES];


int main(int argc, char *argv[]){
    
    int i,j;
    int **population, size, iter,method;
    int best=100000, bestpop[N_CITIES+2];
    
    if (argc<4
        || sscanf(argv[1],"%d",&size)!=1
        || sscanf(argv[2],"%d",&iter)!=1
        || sscanf(argv[3],"%d",&method)!=1) {
        fprintf(stderr,"%s: size iterations method\n",argv[0]);
        return 1;
    }
    
    if (size%4!=0) {
        fprintf(stderr,"%s: size must be multiple of 4\n",argv[0]);
        return 1;
    }
    
    population=(int **)malloc(size*sizeof(int *));
    for (i=0; i<size; i++) {
        population[i] = (int *)malloc((N_CITIES+2)*sizeof(int));
    }
    
    
    srand((unsigned)time(NULL));
    
    readCities(mat);
    
    initialPop(population, size);
    sortPop(population, size);
    
    for (i=0; i<iter; i++) {
        OX(population, size, method);
        sortPop(population, size);
        PIM(population, size);
        sortPop(population, size);
        
        if (i%1000==0) {
            printf("Iteration: %i\t Best distance: %i\n",i,population[0][N_CITIES+1]);
        }
        
        if (best > population[0][N_CITIES+1]) {
            best=population[0][N_CITIES+1];
            
            for (j=0; j<N_CITIES+2; j++) {
                bestpop[j]=population[0][j];
            }
        }
        
        if (best==16514){
            printf("\nRESULTS \n");
            printPop(population, 1);
            
            for (j=0; j<size; j++) {
                free(population[j]);
            }
            free(population);
            
            return 0;
        }
    }
    
    printf("\nRESULTS \n");
    for (i=0; i<N_CITIES+2; i++) {
        population[0][i]=bestpop[i];
    }
    
    printPop(population, 1);
    
    for (i=0; i<size; i++) {
        free(population[i]);
    }
    free(population);
    
    return 0;    
}
